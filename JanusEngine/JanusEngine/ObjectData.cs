﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
namespace JanusEngine
{
    using SlimDX;
    /// <summary>
    /// Sets data structure for CBuffer
    /// </summary>
    [StructLayout(LayoutKind.Sequential,Pack =16, Size=80)]
    struct ObjectData
    {
        /// <summary>
        /// World View Projection Matrix
        /// </summary>
        public Matrix WVP;
        /// <summary>
        /// Objects Colour to be sent
        /// </summary>
        public Color4 Colour;
    }
}
