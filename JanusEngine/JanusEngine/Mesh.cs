﻿
using System.Collections.Generic;
using SlimDX;
using SlimDX.Direct3D11;
using SlimDX.DXGI;

namespace JanusEngine
{
    class Mesh
    {
        public Vector3[] Positions { get; set; }
        // Constructor 
        // Textures 
        public static Mesh CreateRectangle(float width, float height)
        {
            Vector3[] positions = new Vector3[6];
            positions[0] = new Vector3(width * -0.5f, height * -0.5f, 0.0f);
            positions[1] = new Vector3(width * -0.5f, height * 0.5f, 0.0f);
            positions[2] = new Vector3(width *  0.5f, height * 0.5f, 0.0f);
            
            positions[3] = new Vector3(width * 0.5f, height * 0.5f, 0.0f);
            positions[4] = new Vector3(width * 0.5f, height * -0.5f, 0.0f);
            positions[5] = new Vector3(width * -0.5f,height * -0.5f,0.0f);


            return new Mesh { Positions = positions };
        }
        public static Mesh CreateTriangle(float width, float height)
        {
            Vector3[] positions = new Vector3[6];

            positions[0] = new Vector3(width * -0.5f, height * -0.5f, 0.0f);
            positions[1] = new Vector3(width * -0.5f, height * 0.5f, 0.0f);
            positions[2] = new Vector3(width * 0.5f, height * 0.5f, 0.0f);

            return new Mesh { Positions = positions };
        }
        public static Mesh CreateCircle(float radius, int divisions)
        {
            Vector3[] positions = new Vector3[divisions * 3];

            float divAngle = (2.0f * (float) System.Math.PI) / divisions;
            for (int i = 0; i < divisions; i++)
            {
                float angle = divAngle * i;
                float angle2 = divAngle * (i - 1);

                positions[i * 3 + 0] = new Vector3((float)System.Math.Cos(angle), (float)System.Math.Sin(angle), 0.0f) * radius;
                positions[i * 3 + 1] = new Vector3((float)System.Math.Cos(angle2), (float)System.Math.Sin(angle2), 0.0f) * radius;
                positions[i * 3 + 2] = Vector3.Zero;
            }

            return new Mesh { Positions = positions };
        
        }
        public RenderMesh ToRenderMesh()
        {
            var vertices = new DataStream(Positions.Length * 12, true, true);
            for (int i = 0; i < Positions.Length; i++ )
            {
                vertices.Write(Positions[i]);
            }
            vertices.Position = 0;
            Buffer buf = new Buffer(Renderer.Instance.device, vertices, Positions.Length * 12, ResourceUsage.Default, BindFlags.VertexBuffer, CpuAccessFlags.None, ResourceOptionFlags.None, 0);
            return new RenderMesh{buffer = buf, dataStream = vertices, VertexCount = Positions.Length};
        }
    }
}
