﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JanusEngine
{
    using SlimDX;
    /// <summary>
    /// Mesh Renderer - used to render meshes 
    /// </summary>
    class MeshRenderer : Component
    {
        /// <summary>
        /// Which mesh should be used
        /// </summary>
        public RenderMesh Mesh { get; set; }
        /// <summary>
        /// Render mesh colour
        /// </summary>
        public Color4 Colour { get; set; }

        /// <summary>
        /// Attaches object to component, adds render mesh to render list
        /// </summary>
        public override void OnAttach()
        {
            base.OnAttach();

            // Tell it we want to draw
            Renderer.Instance.AddMeshRenderer(this);
        }
        /// <summary>
        /// Removes mesh from render list and detaches component
        /// </summary>
        public override void OnDetach()
        {
            base.OnDetach();
            Renderer.Instance.RemoveMeshRenderer(this);
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_owner"></param>
       public MeshRenderer(Entity _owner) : base(_owner) { }
    }
}
