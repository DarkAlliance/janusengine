﻿using System;

namespace JanusEngine.Physics
{
    /// <summary>
    /// Defines Geometrical properties of a Circle
    /// </summary>
    class Circle : Shape
    {
        /// <summary>
        /// Radius of a given circle
        /// </summary>
        public float Radius { get; set; }
        /// <summary>
        /// Area of a circle
        /// </summary>
        /// <remarks>Calculated by PI * RADIUS SQUARED e.g. 3.142 * Radius * Radius</remarks>
        public override float Area
        {
            // Area = 3.14 * Radius ^ 2
            get { return (float) Math.PI * Radius * Radius; }
        }
        /// <summary>
        /// Default Constructor for Circle
        /// </summary>
        /// <param name="radius"></param>
        /// <param name="density"></param>
        public Circle(float radius, float density = 1.0f)
        {
            Radius = radius;
            Density = density;
        }
        /// <summary>
        /// Gets the inerta of a circle ( Inertia = Area * Radius ^2 * 0.25 * Mass
        /// </summary>
        public override float Inertia
        {
            get
            {
                return Area * Radius * Radius * 0.25f * Mass;
            }
        }
    }
}
