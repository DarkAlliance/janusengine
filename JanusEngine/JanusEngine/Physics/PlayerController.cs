﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JanusEngine.Physics
{
    class PlayerController : Component
    {
        /// <summary>
        /// Singleton instance
        /// </summary>
        public static PlayerController Instance { get; private set; }
        /// <summary>
        /// Players rigid body
        /// </summary>
        private RigidBody body;
        /// <summary>
        /// Control speed
        /// </summary>
        public float Speed { get; set; }
        /// <summary>
        /// Player controller constructor
        /// </summary>
        /// <param name="_owner"></param>
        public PlayerController(Entity _owner) : base(_owner)
        {
            Instance = this;
        }
        /// <summary>
        /// Attach override
        /// </summary>
        public override void OnAttach()
        {
            Console.WriteLine("Attaching player!\n");
            
            base.OnAttach();

            body = Owner.GetComponent<RigidBody>();
        }
        /// <summary>
        /// Detach override
        /// </summary>
        public override void OnDetach()
        {
            Console.WriteLine("Detaching player!\n");
            base.OnDetach();
        }
        /// <summary>
        /// Applies impulse to player to make it move right
        /// </summary>
        public void MoveRight()
        {
            body.ApplyImpulse(SlimDX.Vector2.UnitX * Speed);
        }
        /// <summary>
        /// Applies impulse to player to make it move left
        /// </summary>
        public void MoveLeft()
        {
            body.ApplyImpulse(SlimDX.Vector2.UnitX * -Speed);
        }
        /// <summary>
        /// Applies impulse to player to make it move upwards
        /// </summary>
        public void Jump()
        {
            //Transform.Position += new SlimDX.Vector2(0.0f, Speed * 5.0f);

            body.ApplyImpulse(SlimDX.Vector2.UnitY * Speed);
            
        }
    }
}
