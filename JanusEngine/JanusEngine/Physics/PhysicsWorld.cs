﻿using SlimDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JanusEngine.Physics
{
    class PhysicsWorld
    {
        /// <summary>
        /// Singleton Pattern so there is only ever one single instance of physics world
        /// </summary>
        private static PhysicsWorld instance;
        /// <summary>
        /// Private counter for console output
        /// </summary>
        private int CollisionCount;
        /// <summary>
        /// Internal Constructor
        /// </summary>
        private PhysicsWorld() { }
        /// <summary>
        /// Singletom implementation of PhysicsWorld
        /// </summary>
        public static PhysicsWorld Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PhysicsWorld();
                }
                return instance;
            }
        }
        /// <summary>
        /// List of all Rigid body objects in the application
        /// </summary>
        private List<RigidBody> objects;

        /// <summary>
        /// Initalize a new Rigid body list
        /// </summary>
        public void Initalize()
        {
            objects = new List<RigidBody>();
        }
        /// <summary>
        /// Update the physics world based upon time
        /// </summary>
        /// <param name="timeStep"></param>
        public void Update(float timeStep)
        {
            // Move all of the rigid body objects in the world
            foreach (RigidBody obj in objects)
            {
                obj.Integrate(timeStep);
            }
            // For each collision pair 
            foreach (CollisionPair pair in GenerateCollisionPairs())
            {
                // Here we can break based on number of collisions 
                // Manifold that is set by Test Collision 
                Manifold manifold;
             
                if (TestCollision(pair.Object1, pair.Object2, out manifold))
                {
                    manifold.Object1 = pair.Object1;
                    manifold.Object2 = pair.Object2;
                    // Resolve collisions here 
                    Console.Write("\rCollisions Processed : {0}", CollisionCount++);
                    ResolveCollision(manifold);
                }
            }
        }
        /// <summary>
        /// Generates collision pairs based on the list of objects in the simulation
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CollisionPair> GenerateCollisionPairs()
        {
            // For each object in the world
            for (int i = 0; i < objects.Count; i++)
            {
                //For each other object in the world
                for (int j = i + 1; j < objects.Count; j++)
                {
                    // Pull out the rigid bodys
                    RigidBody obj1 = objects[i];
                    RigidBody obj2 = objects[j];

                    // No need to test for statics
                    if (!obj1.Static || !obj2.Static)
                    {
                        // Return Collision pair found
                        yield return new CollisionPair { Object1 = obj1, Object2 = obj2 };
                    }
                }
            }
        }
        /// <summary>
        /// Tests for collisions between rigid body objects
        /// </summary>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <param name="manifold"></param>
        /// <returns></returns>
        public bool TestCollision(RigidBody obj1, RigidBody obj2, out Manifold manifold)
        {
            // Extract the two rigid body transforms
            Transform2D transform1 = obj1.Owner.GetComponent<Transform2D>();
            Transform2D transform2 = obj2.Owner.GetComponent<Transform2D>();
            // If the collision pair is Circle To Circle
            if (obj1.Shape is Circle && obj2.Shape is Circle)
            {
                // Map two circles
                Circle a = obj1.Shape as Circle;
                Circle b = obj2.Shape as Circle;
                // Pass the two cirlces into the narrow phase
                return CollisionDetection.CircleToCircle(a, transform1.Position, b, transform2.Position, out manifold);
            }
            // If the two bodies is a rectangle and a circle
            else if (obj1.Shape is Rectangle && obj2.Shape is Circle)
            {
                // Map the rectangle and cicrle
                Rectangle a = obj1.Shape as Rectangle;
                Circle b = obj2.Shape as Circle;
                // Pass the rectangle and circle into the narrow phase
                return CollisionDetection.RectToCircle(a, transform1.Position, b, transform2.Position, out manifold);
            }
            // We must now test for the other way around - if object 1 is circle and object 2 is rect
            else if (obj1.Shape is Circle && obj2.Shape is Rectangle)
            {
                // Map the circle and rectangle
                Circle a = obj1.Shape as Circle;
                Rectangle b = obj2.Shape as Rectangle;
                // Pass the rectangle and circle into narrow phase
                return CollisionDetection.RectToCircle(b, transform2.Position, a, transform1.Position, out manifold);
            }
            // Finally if the two objects are both rectangles
            else if (obj1.Shape is Rectangle && obj2.Shape is Rectangle)
            {
                // Map the rectangles
                Rectangle a = obj1.Shape as Rectangle;
                Rectangle b = obj2.Shape as Rectangle;
                // Pass the two rectangles into the narrow phase
                return CollisionDetection.BoxToBox(a, transform1.Position, b, transform2.Position, out manifold);
            }
            manifold = default(Manifold);
            return false;
        }
        /// <summary>
        /// Resolve collisions based upon the manifold
        /// </summary>
        /// <param name="manifold"></param>
        public void ResolveCollision(Manifold manifold)
        {
            // Linear Projection reduces the penetration by percent - it must be calculated after the impulse is applied
            const float percent = 0.2f;
            const float slop = 0.01f;

            // Calculate relative velocity in terms of the normal direction
            Vector2 relativeVelocity = manifold.Object2.GetContactVelocity(manifold.ContactPoint) - manifold.Object1.GetContactVelocity(manifold.ContactPoint);
            float velAlongNormal = Vector2.Dot(relativeVelocity, manifold.Normal);

            // Do not resolve if velocities are separating
            if (velAlongNormal > 0)
                return;

            // Calculate restitution
            float e = Math.Min(manifold.Object1.Restitution, manifold.Object2.Restitution);

            //  Calculate impulse scalar
            float j = -(1 + e) * velAlongNormal;
            j /= manifold.Object1.InverseMass + manifold.Object2.InverseMass;

            // Apply impulse
            Vector2 impulse = j * manifold.Normal;
            manifold.Object1.ApplyImpulse(impulse * -1.0f,manifold.ContactPoint);
            manifold.Object2.ApplyImpulse(impulse,manifold.ContactPoint);

            // Apply friction to the manifold
            ApplyFriction(manifold);
            // Correct the position to prevent sinking objects due to floating point precision errors
            Vector2 correction = manifold.Normal * Math.Max(manifold.Penetration + slop, 0.0f) / (manifold.Object1.InverseMass + manifold.Object2.InverseMass) * percent;

            // Finally update the objects position after collision
            manifold.Object1.Owner.GetComponent<Transform2D>().Position -= manifold.Object1.InverseMass * correction;
            manifold.Object2.Owner.GetComponent<Transform2D>().Position += manifold.Object2.InverseMass * correction;

        }
        /// <summary>
        /// Applies friction based upon the manifold
        /// </summary>
        /// <param name="manifold"></param>
        public void ApplyFriction(Manifold manifold)
        {
            float e = (float) Math.Sqrt(manifold.Object1.Restitution * manifold.Object2.Restitution + manifold.Object1.Restitution * manifold.Object2.Restitution);
            float j =  -(1.0f + e) ;

            Vector2 relativeVelocity = manifold.Object2.GetContactVelocity(manifold.ContactPoint) - manifold.Object1.GetContactVelocity(manifold.ContactPoint);
            
            // When calculating the friction, take the tangent and inverse force
            Vector2 tangent = relativeVelocity - Vector2.Dot(relativeVelocity, manifold.Normal) * manifold.Normal;
            tangent.Normalize();
            float a = RigidBody.Cross(manifold.ContactPoint - manifold.Object1.Owner.GetComponent<Transform2D>().Position, tangent);
            float b = RigidBody.Cross(manifold.ContactPoint - manifold.Object2.Owner.GetComponent<Transform2D>().Position, tangent);

            j /= (manifold.Object1.InverseMass + manifold.Object2.InverseMass + a * a * manifold.Object1.InverseInertia + b * b * manifold.Object2.InverseInertia);

            // Clamp magnitude of friction and create an friction impulse 
            float magnitude = -Vector2.Dot(relativeVelocity, tangent);
            magnitude = magnitude / (1 / manifold.Object1.Mass + 1 / manifold.Object2.Mass);

            float coefficient = (float)Math.Sqrt(manifold.Object1.Friction * manifold.Object1.Friction + manifold.Object2.Friction * manifold.Object2.Friction);

            Vector2 frictionImpulse;
            if(Math.Abs(magnitude) < j * coefficient)
            {
                frictionImpulse = magnitude * tangent;
            }
            else
            {
                frictionImpulse = -j * tangent * coefficient;
            }

            // Apply friction to the objects taking into account the contact point 
            manifold.Object1.ApplyImpulse (manifold.Object1.InverseMass * frictionImpulse,manifold.ContactPoint);
            manifold.Object2.ApplyImpulse (-(manifold.Object2.InverseMass) * frictionImpulse, manifold.ContactPoint);
        }
        /// <summary>
        /// Shutsdown and cleans up physics world
        /// </summary>
        public void Shutdown()
        {
            // Remove all objects in the world 
            foreach (RigidBody obj in objects)
            {
                RemoveObject(obj);
            }
        }
        /// <summary>
        /// Adds a Rigid Body object into the list of objects in the physiscs world
        /// </summary>
        /// <param name="obj"></param>
        public void AddObject(RigidBody obj)
        {
            objects.Add(obj);
        }
        /// <summary>
        /// Removes a Rigid body object from the list of objects in the physics world
        /// </summary>
        /// <param name="obj"></param>
        public void RemoveObject(RigidBody obj)
        {
            objects.Remove(obj);
        }
    }
}

