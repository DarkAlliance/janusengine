﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JanusEngine.Physics
{
    using SlimDX;
    /// <summary>
    /// Manifold Class
    /// </summary>
    public struct Manifold
    {
        /// <summary>
        ///  Two objects that are colliding
        /// </summary>
        public RigidBody Object1, Object2;
        /// <summary>
        /// Direction of the collision 
        /// </summary>
        public Vector2 Normal;
        /// <summary>
        /// How far the two objects intersect
        /// </summary>
        public float Penetration;
        /// <summary>
        /// Point of Contact
        /// </summary>
        public Vector2 ContactPoint;
    }
    /// <summary>
    /// Collision Pair - Two Objects that have the potential to collide
    /// </summary>
    public struct CollisionPair
    {
        /// <summary>
        /// Two colliding objects
        /// </summary>
        public RigidBody Object1, Object2; 
    }
}
