﻿using System;
using SlimDX;

namespace JanusEngine.Physics
{
    /// <summary>
    /// Defines Geometrical Properties of a Rectangle
    /// </summary>
    class Rectangle : Shape
    {
        /// <summary>
        /// Width of the rectangle X axis aligned
        /// </summary>
        public float Width { get; set; }
        /// <summary>
        /// Height of the rectangle Y axis aligned
        /// </summary>
        public float Height { get; set; }
        /// <summary>
        /// Calculate area of the rectangle
        /// </summary>
        /// <remarks> Area = Width * Height</remarks>
        /// <returns>float</returns>
        public override float Area
        {
            // Area = Width * Height
            get { return Width * Height; }
        }
        /// <summary>
        /// Default constructor of a rectangle shape
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="density"></param>
        public Rectangle(float width,float height, float density = 1.0f)
        {
            // Set the values 
            Width = width;
            Height = height;
            Density = density;
        }
        /// <summary>
        /// Gets the inertia of a Rectangle
        /// </summary>
        public override float Inertia
        {
            get
            {
                return 0;
                //return Area * Mass * 0.50f;
            }
        }
    }
}
