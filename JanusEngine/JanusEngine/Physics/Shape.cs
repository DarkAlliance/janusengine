﻿using System;
using SlimDX;
namespace JanusEngine.Physics
{
    /// <summary>
    /// Shape Base class - Represents basic properties of a shape
    /// </summary>
    public abstract class Shape
    {
       /// <summary>
       /// Property to store the Area of a given shape
       /// </summary>
       public abstract float Area{get;}
       /// <summary>
       /// Density of a shape
       /// </summary>
       public float Density { get; set; }
       /// <summary>
       ///  Mass of a Shape - Calculated by Area * Density
       /// </summary>
       public float Mass
       {
           get
           {
               return Area * Density;
           }
       }
        /// <summary>
        /// Get the inertia
        /// </summary>
       public virtual float Inertia { get { return Mass; } }

    }
}
