﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JanusEngine.Physics
{
   using SlimDX;
    /// <summary>
    /// Defines a Rigid body object component
    /// </summary>
   public class RigidBody : Component
    {
       /// <summary>
       /// Define the owner of the rigid body component
       /// </summary>
       /// <param name="_owner"></param>
        public RigidBody(Entity _owner) : base(_owner) { }
       /// <summary>
       /// Cahce the shape of the rigid body
       /// </summary>
        public Shape Shape { get; set; }
       /// <summary>
       /// Internal variables for mass and inverse mass
       /// </summary>
        private float mass, inverseMass;
       /// <summary>
       /// Public getter for inverse mass
       /// </summary>
        public float InverseMass
        {
            get { return inverseMass; }
         
        }
        /// <summary>
        /// How stiff the rotational mass is
        /// </summary>
       public float inertia, inverseInertia;
       /// <summary>
       /// Accessors for Inertia
       /// </summary>
        public float Inertia
        {
            get { return inertia; }
            set { inertia = value; }
        }
       /// <summary>
       /// Accessors for inverse inertia
       /// </summary>
        public float InverseInertia
        {
            get { return inverseInertia; }
            set { inverseInertia = value; }
        }
       /// <summary>
       /// Public getter for mass
       /// </summary>
        public float Mass
        {
            get { return mass; }
            
        }
       /// <summary>
       /// Define if the rigid body is static - It should never move
       /// </summary>
        public bool Static { get; set; }
       /// <summary>
       /// Defines if the rigid body is influenced by gravity
       /// </summary>
        public bool GravityInfluence { get; set; }
       /// <summary>
       /// Velocity of the rigid body
       /// </summary>
        public Vector2 Velocity { get; private set; }
       /// <summary>
       /// The speed in which the rigid body rotates
       /// </summary>
        public float RotationVelocity { get; set; }
       /// <summary>
       /// Law of Restitution - Loss based recovery 
       /// </summary>
        public float Restitution { get; set; }
       /// <summary>
       /// Value of friction
       /// </summary>
        public float Friction { get; set; }
       /// <summary>
       /// Attach to the component system
       /// </summary>
        public override void OnAttach()
        {
            base.OnAttach();
            if (Static)
            {
                mass = 0.0f;
                inverseMass = 0.0f;
                inertia = 0.0f;
                inverseInertia = 0.0f;
            }
            else
            {
                // Define the mass and inverse mass
                mass = Shape.Mass;
                inverseMass = 1.0f / mass;
                inertia = Shape.Inertia;
                if (Shape.Inertia != 0.0f)
                    inverseInertia = 1.0f / Shape.Inertia;
            }
            // Add the new rigid body object to the world
            PhysicsWorld.Instance.AddObject(this);
        
        }
       /// <summary>
       /// Apply s an impulse vector upon a rigid body object
       /// </summary>
       /// <param name="impulse"></param>
        public void ApplyImpulse(Vector2 impulse)
        {
            Velocity += impulse * inverseMass;
        }
       /// <summary>
       /// Apply impulse for rotations
       /// </summary>
       /// <param name="impulse"></param>
       /// <param name="origin"></param>
        public void ApplyImpulse(Vector2 impulse,Vector2 origin)
        {
            Velocity += impulse * inverseMass;
            // Translate into origin from contact point
            RotationVelocity += Cross(origin - Owner.GetComponent<Transform2D>().Position, impulse) * InverseInertia;
        }
       /// <summary>
       ///  Gets the velocity of the object in world space 
       /// </summary>
       /// <param name="origin"></param>
       /// <returns></returns>
       public Vector2 GetContactVelocity(Vector2 origin)
        {
            return Velocity + Cross(RotationVelocity, origin - Owner.GetComponent<Transform2D>().Position);
        }
       /// <summary>
       /// Apply Drag force such as gravity
       /// </summary>
       /// <returns></returns>
        public Vector2 DragForce()
        {
            // If we are affected by gravity 
            if(GravityInfluence)
            {
                // Apply Gravity
                return new Vector2(0.0f, -9.80665f); 
            }
            // Do nothing
            return new Vector2(0.0f,0.0f);
        }
       /// <summary>
       /// Integrate the movement of the rigid body
       /// </summary>
       /// <param name="timeStep"></param>
        public void Integrate(float timeStep)
        {
            // If the object is static, exit straight away given that it will never move
            if(Static)
                return;
            // Get the transformation of the rigid body (mainly for position)
            Transform2D transform = Owner.GetComponent<Transform2D>();
            // Apply Drag Force
            Vector2 Acceleration = DragForce();
            // Integrate Position with velocity
            Velocity += Acceleration * timeStep;
            transform.Position += Velocity * timeStep;
            // Just for angles 
            transform.Rotation += RotationVelocity * timeStep; 
        }
       /// <summary>
       /// Calculate cross product
       /// </summary>
       /// <param name="left"></param>
       /// <param name="right"></param>
       /// <returns></returns>
        public static float Cross(Vector2 left, Vector2 right)
        {
            return left.X * right.Y - left.Y * right.X;
        }
        /// <summary>
        /// Calculate cross product
        /// </summary>
        /// <param name="left"></param>
        /// <param name="scale"></param>
        /// <returns></returns>
       public static Vector2 Cross(Vector2 left, float scale)
        {
            return new Vector2(scale * left.Y, -scale * left.X);
        }
       /// <summary>
       /// Calculate cross product
       /// </summary>
       /// <param name="scale"></param>
       /// <param name="right"></param>
       /// <returns></returns>
        public static Vector2 Cross(float scale, Vector2 right)
        {
            return new Vector2(-scale * right.Y, scale * right.X);
        }
       /// <summary>
       /// Detach the object from the component system
       /// </summary>
        public override void OnDetach()
        {
            base.OnDetach();
            // Remove this rigid body object from the physics world
            PhysicsWorld.Instance.RemoveObject(this);
        }

     
    }
}
