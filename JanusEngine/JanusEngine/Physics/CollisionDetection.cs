﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JanusEngine.Physics
{
    using SlimDX;
    /// <summary>
    /// Collection of methods to detect collisions between geometry
    /// </summary>
    class CollisionDetection
    {
        /// <summary>
        /// Detects Collisions between Circle vs Circle
        /// </summary>
        /// <param name="circle1"></param>
        /// <param name="pos1"></param>
        /// <param name="circle2"></param>
        /// <param name="pos2"></param>
        /// <param name="manifold"></param>
        /// <returns></returns>
        public static bool CircleToCircle(Circle circle1, Vector2 pos1, Circle circle2, Vector2 pos2, out Manifold manifold)
        {
            // Calculate the vector between Circle1 one and Circle2
            Vector2 between = pos2 - pos1;
            // Caclulate the total radius 
            float totalRadius = circle1.Radius + circle2.Radius;
            float lenghSquared = between.LengthSquared();
            // If the length is more than the total radius ^2 then there is no collision found
            if (lenghSquared > totalRadius * totalRadius)
            {
                // Exit now as no collision was found
                manifold = default(Manifold);
                return false;
            }
            // Otherwise calculate the expensive square root
            float length = (float)Math.Sqrt(lenghSquared);
            // Normalize the between vector
            between /= length;

            // Set the manifold properties 
            manifold = new Manifold();
            manifold.Normal = between;

            // How much there intersecting 
            manifold.Penetration = totalRadius - length;
            // Calculate the contact point 
            manifold.ContactPoint = pos1 + (circle1.Radius - (manifold.Penetration * 0.5f)) * between;

            // Return true that a collision has been found
            return true;
        }
        /// <summary>
        /// Detects Collisions between Rectangles and Circles
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="pos1"></param>
        /// <param name="circle"></param>
        /// <param name="pos2"></param>
        /// <param name="manifold"></param>
        /// <returns>bool</returns>
        public static bool RectToCircle(Rectangle rect, Vector2 pos1, Circle circle, Vector2 pos2, out Manifold manifold)
        {

            // Vector between A to B
            Vector2 between = pos2 - pos1;
            manifold = default(Manifold);

            // Closest point on Rectangle A to center of Circle B
            Vector2 closest = between;

            // Calculate half extents along each axis
            Vector2 extent;
            extent.X = rect.Width / 2.0f;
            extent.Y = rect.Height / 2.0f;

            // Clamp to the closest edge
            closest = Vector2.Clamp(closest, -extent, extent);
            bool inside = false;
            // If circle is already inside the rect we need to clamp the cirle to the closest rectangle edge
            if (between == closest)
            {
                inside = true;
                // Find the closest axis
                if (Math.Abs(between.X) > Math.Abs(between.Y))
                {
                    // Clamp to the closest extent if it is X axis
                    if (closest.X > 0)
                        closest.X = extent.X;
                    else
                        closest.X = -extent.X;
                }
                else
                {
                    // Clamp to closest edge if it is Y axis
                    if (closest.Y > 0)
                        closest.Y = extent.Y;
                    else
                        closest.Y = -extent.Y;
                }
            }
            closest += pos1;
            // B pos - Closest
            Vector2 normal = pos2 - closest;
            double d = normal.LengthSquared();
            float radius = circle.Radius;
            // Exit if the radius is shorter than the distance to the closest point and we're not inside the rectangle
            if (d > radius * radius && !inside)
                return false;

            // :( Sqrt 
            d = Math.Sqrt(d);

            // Normalise the distance
            normal /= (float)d;
            // If we are too far in
            if (inside)
            {
                // Set the manifold properties
                manifold.Normal = normal;
                manifold.Penetration = radius + (float)d;
            }
            else
            {
                // Set the manifold properties
                manifold.Normal = -normal;
                manifold.Penetration = radius - (float)d;
            }
            manifold.ContactPoint = closest;
            // Return true that a collision has been found 
            return true;
        }
        /// <summary>
        /// Detects Collisions Between a Rectangle vs Rectangle
        /// </summary>
        /// <param name="rect1"></param>
        /// <param name="pos1"></param>
        /// <param name="rect2"></param>
        /// <param name="pos2"></param>
        /// <param name="manifold"></param>
        /// <returns></returns>
        public static bool BoxToBox(Rectangle rect1, Vector2 pos1, Rectangle rect2, Vector2 pos2, out Manifold manifold)
        {
            manifold = default(Manifold);
            // Vector between A to B
            Vector2 between = pos2 - pos1;

            // Calculate half extents along each axis
            Vector2 pos1Extent;
            pos1Extent.X = rect1.Width / 2.0f;
            pos1Extent.Y = rect1.Height / 2.0f;
            // Calculate half extents along each axis
            Vector2 pos2Extent;
            pos2Extent.X = rect2.Width / 2.0f;
            pos2Extent.Y = rect2.Height / 2.0f;

            // Calculate overlap on X axis 
            float x_overlap = pos1Extent.X + pos2Extent.X - Math.Abs(between.X);
            // If overlap on the X axis is found
            if (x_overlap > 0)
            {
                float pos1YExtent = (rect1.Height - rect2.Height) / 2.0f;

                float y_overlap = pos1Extent.Y + pos2Extent.Y - Math.Abs(between.Y);
                // If overlap on Y axis is found
                if (y_overlap > 0)
                {
                    // Find out which axis is least penetrating to deterimine the contact normal
                    if (x_overlap < y_overlap)
                    {
                        // If overlap is found on the X axis - Set manifold properties
                        if (between.X < 0)
                            manifold.Normal = new Vector2(-1.0f, 0.0f);
                        else
                        {
                            manifold.Normal = Vector2.UnitX;
                            manifold.Penetration = x_overlap;
                        }
                        return true;
                    }
                    else
                    {
                        // If overlap is found on the Y axis - Set manifold properties
                        if (between.Y < 0.0f)
                            manifold.Normal = new Vector2(0.0f, -1.0f);
                        else
                        {
                            manifold.Normal = new Vector2(0.0f, 1.0f);
                            manifold.Penetration = y_overlap;
                        }
                    }
                    return true;

                }


            }

            return false;
        }
    }
}
