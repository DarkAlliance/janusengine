﻿using System.Windows.Forms;
using SlimDX;
using SlimDX.D3DCompiler;
using SlimDX.Direct3D11;
using SlimDX.DXGI;
using System.IO;
using SlimDX.Windows;
using Device = SlimDX.Direct3D11.Device;
using Resource = SlimDX.Direct3D11.Resource;
using System.Collections.Generic;
using System.Diagnostics;

namespace JanusEngine
{
    class Renderer 
    {
        /// <summary>
        /// Singletom instance of Renderer
        /// </summary>
        private static Renderer instance;
        /// <summary>
        /// List of objects to be rendered
        /// </summary>
        private IList<MeshRenderer> renderList;
        /// <summary>
        /// Default Constructor
        /// </summary>
        private Renderer() { }
        /// <summary>
        /// Singleton implementation of Renderer
        /// </summary>
        public static Renderer Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Renderer();
                }
                return instance;
            }
        }
        /// <summary>
        /// Hardware Device
        /// </summary>
        public Device device;
        /// <summary>
        /// Device Context
        /// </summary>
        public DeviceContext context;
        /// <summary>
        /// Screen viewport
        /// </summary>
        public Viewport viewport;
        /// <summary>
        /// Aspect Ratio of the screen
        /// </summary>
        public float aspectRatio;
        /// <summary>
        /// Direct X Swap chain
        /// </summary>
        SwapChain swapChain;
        /// <summary>
        /// Shader Signitures
        /// </summary>
        ShaderSignature inputSignature;
        /// <summary>
        /// Render Window / Form
        /// </summary>
        RenderForm form;
        /// <summary>
        /// Default Render Target View
        /// </summary>
        RenderTargetView renderTarget;
        /// <summary>
        /// Default Vertext Shader
        /// </summary>
        private VertexShader vertexShader;
        /// <summary>
        /// Default Pixel Shader
        /// </summary>
        private PixelShader pixelShader;
        /// <summary>
        /// Projection Matrix - Projects into NDC
        /// </summary>
        private Matrix projectionMatrix;
        /// <summary>
        /// Sets if Wireframe rasterization should be used
        /// </summary>
        private bool Wireframe { get; set; }
        /// <summary>
        /// Cbuffer used to parse data to Shaders 
        /// </summary>
        private CBuffer<ObjectData> cBuffer;
        /// <summary>
        /// Frame timer - Counts the number of frames
        /// </summary>
        private Stopwatch frameTimer;
        /// <summary>
        /// Physics Accumilation counter
        /// </summary>
        private double physAccum;
        /// <summary>
        /// Physics Frame time locked to 60 FPS
        /// </summary>
        private const double physicsFrameTime = 1.0f / 60.0f;

        /// <summary>
        /// Returns ShaderByteCode
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private ShaderBytecode GetShaderBytecode(string filename)
        {
            if (!File.Exists(filename)) return null;
            // Loads CSO Object
            byte[] raw = File.ReadAllBytes(filename);
           
            // Create new shader bytecode 
            using (DataStream ds = new DataStream(raw,true,true))
            {
                return new ShaderBytecode(ds);
            }
        }
        /// <summary>
        /// Sets up rendering
        /// </summary>
        public void Initialize()
        {
            // Render form window - with title
            form = new RenderForm("Janus Engine");

            // Create a new Render list
            renderList = new List<MeshRenderer>();

            // Create a new Swapchain description to describe how Direct X should be initalized
            var description = new SwapChainDescription()
            {
                BufferCount = 2,
                Usage = Usage.RenderTargetOutput,
                OutputHandle = form.Handle,
                IsWindowed = true,
                ModeDescription = new ModeDescription(0, 0, new Rational(60, 1), Format.R8G8B8A8_UNorm),
                SampleDescription = new SampleDescription(1, 0),
                Flags = SwapChainFlags.AllowModeSwitch,
                SwapEffect = SwapEffect.Discard
            };

            // Actually create the Device - Note this returns into device and swapcahin
            Device.CreateWithSwapChain(DriverType.Hardware, DeviceCreationFlags.Debug, description, out device, out swapChain);

            // create a view of our render target, which is the backbuffer of the swap chain we just created

            using (var resource = Resource.FromSwapChain<Texture2D>(swapChain, 0))
                renderTarget = new RenderTargetView(device, resource);


            // Setting device context and viewport 
             context = device.ImmediateContext;
            viewport = new Viewport(0.0f, 0.0f, form.ClientSize.Width, form.ClientSize.Height);
            context.OutputMerger.SetTargets(renderTarget);
            context.Rasterizer.SetViewports(viewport);

            // Calculate Aspect ratio
            aspectRatio = (float)form.ClientSize.Width / form.ClientSize.Height;
            // Setup Projection Matrix 
            projectionMatrix = Matrix.OrthoLH(20.0f * aspectRatio, 20.0f, 0.0f, 1.0f);

            // load and compile the vertex shader
            using (var bytecode = GetShaderBytecode("basic_shader_vs.cso"))
            {
                inputSignature = ShaderSignature.GetInputSignature(bytecode);
                vertexShader = new VertexShader(device, bytecode);
            }

            // load and compile the pixel shader
            using (var bytecode = GetShaderBytecode("basic_shader_ps.cso"))
                pixelShader = new PixelShader(device, bytecode);

            // create the vertex layout and buffer
            var elements = new[] { new InputElement("POSITION", 0, Format.R32G32B32_Float, 0) };
            var layout = new InputLayout(device, inputSignature, elements);
           
            // Setup Cbuffers
            cBuffer = new CBuffer<ObjectData>(new ObjectData
            {
                WVP = Matrix.Identity,
                Colour = new Color4(0.0f,0.0f,1.0f,1.0f)
                
            });

            // configure the Input Assembler portion of the pipeline with the vertex data
            context.InputAssembler.InputLayout = layout;
            context.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;

            // set the shaders
            context.VertexShader.Set(vertexShader);
            context.PixelShader.Set(pixelShader);

            // Set the CBuffer
            context.VertexShader.SetConstantBuffer(cBuffer.buffer, 0);
            context.PixelShader.SetConstantBuffer(cBuffer.buffer, 0);

            // prevent DXGI handling of alt+enter, which doesn't work properly with Winforms
            using (var factory = swapChain.GetParent<Factory>())
                factory.SetWindowAssociation(form.Handle, WindowAssociationFlags.IgnoreAltEnter);

            form.KeyDown += (o,e) =>
                {
                    // Handle Full Screen Hot key Alt + Enter
                    if (e.Alt && e.KeyCode == Keys.Enter)
                        swapChain.IsFullScreen = !swapChain.IsFullScreen;

                    // Handle player movement (WASD) / LEFT RIGHT UP DOWN KEYS
                    if (e.KeyCode == Keys.Left || e.KeyCode == Keys.A)
                        Physics.PlayerController.Instance.MoveLeft();
                    else if (e.KeyCode == Keys.Right || e.KeyCode == Keys.D)
                        Physics.PlayerController.Instance.MoveRight();
                    else if (e.KeyCode == Keys.Up || e.KeyCode == Keys.W)
                        Physics.PlayerController.Instance.Jump();
                    else if (e.KeyCode == Keys.Space)
                        Physics.PlayerController.Instance.Jump();
                    // Handle Wireframe key (R)
                    else if (e.KeyCode == Keys.R)
                    {
                        Wireframe = !Wireframe;
                        SetWireFrame();
                    }

                };
            // handle form size changes
            form.UserResized += (o, e) =>
            {
                // Clear the render target view
                renderTarget.Dispose();
                // Resize swapchain buffers
                swapChain.ResizeBuffers(2, 0, 0, Format.R8G8B8A8_UNorm, SwapChainFlags.AllowModeSwitch);
                // Setup the newly resized render target 
                using (var resource = Resource.FromSwapChain<Texture2D>(swapChain, 0))
                    renderTarget = new RenderTargetView(device, resource);
                // Pass this onto the output merger
                context.OutputMerger.SetTargets(renderTarget);
            };

            // Create a new Stopwatch and start the timer
            frameTimer = new Stopwatch();
            frameTimer.Start();  
        }
        /// <summary>
        /// Add Mesh renderer - Adds mesh object to render list
        /// </summary>
        /// <param name="meshRenderer"></param>
        public void AddMeshRenderer(MeshRenderer meshRenderer)
        {
            renderList.Add(meshRenderer);
        }
        /// <summary>
        /// Remove Mesh Renderer - Removes mesh object from render list
        /// </summary>
        /// <param name="renderer"></param>
        public void RemoveMeshRenderer(MeshRenderer renderer)
        {
            renderList.Remove(renderer);
        }
        /// <summary>
        /// Update DirectX
        /// </summary>
        public void Run()
        {
            MessagePump.Run(form, Update);   
        }
        /// <summary>
        /// Update all graphical elements and physics objects
        /// </summary>
        private void Update()
        {

            // clear the render target to a soothing blue
            context.ClearRenderTargetView(renderTarget, new Color4(0.5f, 0.5f, 1.0f));
            // set frame time to that of the total elapsed seconds
            double frameTime = frameTimer.Elapsed.TotalSeconds;
            // Restart the timer as we have a new private varible (frameTime)
            frameTimer.Restart();
            // Accumulate the frameTime
            physAccum += frameTime;
            // If the frame time is more that the limit of 60 fps
            while(physAccum > physicsFrameTime)
            {
                // De-count
                physAccum -= physicsFrameTime;
                // Update the physics world
                Physics.PhysicsWorld.Instance.Update((float)physicsFrameTime);
            }
            // For each object in render list 
            for (int i = 0; i < renderList.Count; i++)
            {
                // Get the current messh to render
                MeshRenderer renderer = renderList[i];
                ObjectData objData = new ObjectData
                {   // Set the object CBuferr that is sent to the shader
                    Colour = renderer.Colour,
                    WVP = renderer.Owner.GetComponent<Transform2D>().LocalToWorld * projectionMatrix 
                };
                cBuffer.Set(objData);
                // Bind the vertex buffers
                context.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(renderer.Mesh.buffer, sizeof(float) * 3, 0));
                // draw the triangle
                context.Draw(renderer.Mesh.VertexCount, 0);
            }
            // Present
            swapChain.Present(0, PresentFlags.None);
        }
        /// <summary>
        /// Turns Wireframe mode On/OFF
        /// </summary>
        public void SetWireFrame()
        {
            // If Wire frame is true, set the Fill Mode to Wireframe
            if (Wireframe)
            {
                RasterizerStateDescription WireFrame = new RasterizerStateDescription()
                {
                    CullMode = CullMode.None,
                    DepthBias = 0,
                    DepthBiasClamp = 0.0f,
                    FillMode = FillMode.Wireframe,
                    IsAntialiasedLineEnabled = false,
                    IsDepthClipEnabled = false,
                    IsFrontCounterclockwise = false,
                    IsMultisampleEnabled = false,
                    IsScissorEnabled = false,
                    SlopeScaledDepthBias = 0.0f
                };
                // Update the device rasterizer state
                RasterizerState rs = RasterizerState.FromDescription(device, WireFrame);
                device.ImmediateContext.Rasterizer.State = rs;
            }
            else
            {
                // Turn Wireframe off
                RasterizerStateDescription NoWireFrame = new RasterizerStateDescription()
                {
                    CullMode = CullMode.Back,
                    DepthBias = 0,
                    DepthBiasClamp = 0.0f,
                    FillMode = FillMode.Solid,
                    IsAntialiasedLineEnabled = false,
                    IsDepthClipEnabled = false,
                    IsFrontCounterclockwise = false,
                    IsMultisampleEnabled = false,
                    IsScissorEnabled = false,
                    SlopeScaledDepthBias = 0.0f
                };
                // Update device rasterizer state
                RasterizerState rs = RasterizerState.FromDescription(device, NoWireFrame);
                device.ImmediateContext.Rasterizer.State = rs;
            }
        }
        /// <summary>
        /// Cleanup any scene resources and shutdown application
        /// </summary>
        public void Shutdown()
        {
            inputSignature.Dispose();
            vertexShader.Dispose();
            pixelShader.Dispose();
            renderTarget.Dispose();
            swapChain.Dispose();
            device.Dispose();
            Application.Exit();
        }
        
    }
}
