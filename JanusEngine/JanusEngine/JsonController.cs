﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JanusEngine
{
    public class SceneObject
    {
        public string ObjName;
        public SlimDX.Vector3 Position;
    }
  public  class JsonController
    {

        public string FilePath { get; set; }
        public SceneObject obj;
       
        public void ReadFromFile()
        {
            SceneObject temp = JsonConvert.DeserializeObject<SceneObject>(File.ReadAllText(FilePath));
            Console.WriteLine("Temp properties :" + temp.Position.ToString() + "\n" + temp.ObjName.ToString());
        }
        public void WriteToFile() {
            obj.ObjName = "Test";
            obj.Position = new SlimDX.Vector3(1.0f, 0.0f, 0.0f);
            Console.WriteLine("Starting JSON write.....\n");
            File.WriteAllText(FilePath, JsonConvert.SerializeObject(obj));
            using (StreamWriter file = File.CreateText(FilePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, obj);
            }
        }
    }
}
