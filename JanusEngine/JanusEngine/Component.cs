﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JanusEngine
{
    /// <summary>
    /// Component base class
    /// </summary>
    public abstract class Component
    {
        /// <summary>
        /// Entity cache
        /// </summary>
        public Entity Owner { get; private set; }
        /// <summary>
        /// Override Attach called when entity is constructed
        /// </summary>
        public virtual void OnAttach() { }
        /// <summary>
        /// Override Attach called when entity is deconstructed
        /// </summary>
        public virtual void OnDetach() { }
        /// <summary>
        /// Entity Constructor
        /// </summary>
        /// <param name="_entity"></param>
        public Component(Entity _entity) { Owner = _entity; }
    }
}