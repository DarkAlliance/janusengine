﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
namespace JanusEngine
{
    using SlimDX;
    using SlimDX.Direct3D11;
    class CBuffer<T> where T : struct
    {

        private bool disposed;

        public Buffer buffer { get; private set; }
        private DataStream dataStream { get; set; }
        private int size;

        public CBuffer(T init)
        {
            size = Marshal.SizeOf(typeof(T));
            dataStream = new DataStream(size, true, true);
            buffer = new Buffer(Renderer.Instance.device, new BufferDescription
            {
                BindFlags = BindFlags.ConstantBuffer,
                SizeInBytes = size,
                Usage = ResourceUsage.Default
            });
            Set(init);
        }
        public void Set(T value)
        {
            dataStream.Position = 0;
            dataStream.Write(value);
            dataStream.Position = 0;
            Renderer.Instance.context.UpdateSubresource(new DataBox(0, 0, dataStream), buffer, 0);
        }
        ~CBuffer()
        {
            Dispose();
        }
        public void Dispose()
        {
            if(!disposed)
            {
                buffer.Dispose();
                dataStream.Dispose();
                disposed = true;
            }
        }
    }
}
