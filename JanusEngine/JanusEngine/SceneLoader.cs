﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Reflection;

namespace JanusEngine
{

    class SceneLoader
    {
        private Dictionary<string, Type> compType;

        private static SceneLoader instance;
        public SceneLoader()
        {
            compType = new Dictionary<string, Type>();


            Type cType = typeof(Component);
            foreach (Type type in AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany((a)
                => a.GetTypes())
                .Where((t) => !t.IsAbstract && cType.IsAssignableFrom(t)))
            {
                compType.Add(type.Name, type);
                Console.WriteLine("Component found " + type.Name);
            }
        }
        public static SceneLoader Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SceneLoader();
                }
                return instance;
            }
        }
        public void LoadScene(string fileName)
        {
            if (!File.Exists(fileName)) throw new Exception(fileName + " Not found!");
            JObject root = JObject.Parse(File.ReadAllText(fileName));

            foreach (JProperty jObj in root.Cast<JProperty>())
            {
                Entity ent = new Entity(jObj.Name);
                Console.WriteLine("Entity found" + ent.Name);

                JObject jent = jObj.Value as JObject;

                foreach (JProperty jComp in jent.Cast<JProperty>())
                {
                    Type type;
                    if (compType.TryGetValue(jComp.Name, out type))
                    {
                        Component component = ent.AddComponent(type);
                        JObject jComp2 = jComp.Value as JObject;
                        foreach (JProperty jattr in jComp2.Cast<JProperty>())
                        {
                            PropertyInfo pInfo = type.GetProperty(jattr.Name, BindingFlags.Public | BindingFlags.Instance);
                            if (pInfo != null)
                            {
                                object obj = ConvertToken(jattr.Value, pInfo.PropertyType);
                                if(obj != null)
                                {
                                    pInfo.SetValue(component, obj);
                                }
                                else
                                {

                                    Console.Write("Unkown value {0} to {1}", jattr.Value, pInfo.PropertyType.Name);
                                }
                            }
                            else
                            {
                                Console.Write("Unknown property {0} on component {1}", jattr.Name, type.Name);
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Unknown Component {0} found.\n", jComp.Name);
                    }
                }
                ent.Finish();

            }
        }
        private object ConvertToken(JToken token, Type desiredType)
        {
            switch(desiredType.Name)
            {
                case "Vector2":
                    if(token.Type == JTokenType.Array)
                    {
                        JArray jar = token as JArray;
                        return new SlimDX.Vector2((float)ConvertToken(jar[0],typeof(float)), (float)ConvertToken(jar[1],typeof(float)));
                    }
                    else
                    {
                        return null;
                    }
                case "Single":
                    if (token.Type == JTokenType.Float)
                    {
                        return (float)token;
                    }
                    else if (token.Type == JTokenType.Integer)
                    {
                        return (float)(int)token;
                    }
                    else
                    {
                        return null;
                    }
                   

                case "Vector3":
                    if (token.Type == JTokenType.Array)
                    {
                        JArray jar = token as JArray;
                        return new SlimDX.Vector3((float)ConvertToken(jar[0], typeof(float)), (float)ConvertToken(jar[1], typeof(float)), (float)ConvertToken(jar[2], typeof(float)));
                    }
                    else
                    {
                        return null;
                    }
                case "Color4" :

                    if (token.Type == JTokenType.Array)
                    {
                        JArray jar = token as JArray;
                        return new SlimDX.Color4((float)ConvertToken(jar[0], typeof(float)), (float)ConvertToken(jar[1], typeof(float)), (float)ConvertToken(jar[2], typeof(float)), (float)ConvertToken(jar[3], typeof(float)));
                    }
                    else
                    {
                        return null;
                    }
                case "Vector4":

                    if (token.Type == JTokenType.Array)
                    {
                        JArray jar = token as JArray;
                        return new SlimDX.Vector4((float)ConvertToken(jar[0], typeof(float)), (float)ConvertToken(jar[1], typeof(float)), (float)ConvertToken(jar[2], typeof(float)), (float)ConvertToken(jar[3], typeof(float)));
                    }
                    else
                    {
                        return null;
                    }
                case "Shape":
                    if(token.Type == JTokenType.Object)
                    {
                        JObject job = token as JObject;

                        string meshType = (string)job["Type"];
                        if(meshType  == "Circle")
                        {
                            float radius = (float)job["Radius"];
                            return new Physics.Circle(radius);

                        }
                        else if (meshType == "Rectangle")
                        {
                            float width = (float)job["Width"];
                            float height = (float)job["Height"];
                            return new Physics.Rectangle(width, height);

                        }
                        else 
                        {
                            Console.WriteLine("Error!");
                            return null;
                        }
                     
                    }
                    else
                        return null;
                case "RenderMesh":
                     if(token.Type == JTokenType.Object)
                    {
                        JObject job = token as JObject;

                        string meshType = (string)job["Type"];
                        if(meshType  == "Circle")
                        {
                            float radius = (float)job["Radius"];
                            return Mesh.CreateCircle(radius, 25).ToRenderMesh();

                        }
                        else if (meshType == "Rectangle")
                        {
                            float width = (float)job["Width"];
                            float height = (float)job["Height"];
                            return Mesh.CreateRectangle(width,height).ToRenderMesh();

                        }
                        else 
                        {
                            Console.WriteLine("Error!");
                            return null;
                        }
                     
                    }
                    else
                        return null;
                case "Int32":
                    if (token.Type == JTokenType.Integer)
                    {
                        return (int)token;
                    }
                    else if (token.Type == JTokenType.Float)
                    {
                        return (int)(float)token;
                    }
                    else
                    {
                        return null;
                    }
                   
                case "Boolean":
                    if(token.Type == JTokenType.Boolean)
                    {
                        return (bool)token;
                    }
                    else
                    {
                        return null;
                    }
                default:
                    return null;
            }
        }
    }

}
