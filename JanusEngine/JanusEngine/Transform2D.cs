﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JanusEngine
{
    using SlimDX;
    /// <summary>
    /// Creates a Transform 2D Component
    /// </summary>
    class Transform2D : Component
    {
        /// <summary>
        /// Position of the component
        /// </summary>
        public Vector2 Position { get; set; }
        /// <summary>
        /// Rotation of the component
        /// </summary>
        public float Rotation { get; set; }
        /// <summary>
        /// Scale of the component
        /// </summary>
        public Vector2 Scale { get; set; }
        /// <summary>
        /// Local to world matrix
        /// </summary>
        public Matrix LocalToWorld
        {
            get
            {
                return Matrix.Scaling(new Vector3(Scale, 0.0f)) * Matrix.RotationAxis(Vector3.UnitZ, Rotation) * Matrix.Translation(new Vector3(Position, 0.0f));
            }
        }
        /// <summary>
        /// World to local matrix
        /// </summary>
        public Matrix WorldToLocal
        {
            get
            {
                Matrix mtx = LocalToWorld;
                mtx.Invert();
                return mtx;
            }
        }
        /// <summary>
        /// Transform 2D constructor
        /// </summary>
        /// <param name="_owner"></param>
        public Transform2D(Entity _owner) : base(_owner) { }
    }
}
