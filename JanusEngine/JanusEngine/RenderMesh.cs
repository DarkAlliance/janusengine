﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace JanusEngine
{
    using SlimDX;
    using SlimDX.Direct3D11;

    class RenderMesh : IDisposable
    {
        private bool disposed;

        public Buffer buffer { get; set; }
        public DataStream dataStream { get; set; }

        public int VertexCount { get; set; }
      
        ~RenderMesh()
        {
            Dispose();
        }
        public void Dispose()
        {
            if(!disposed)
            {
                buffer.Dispose();
                dataStream.Dispose();
                disposed = true;
            }
        }
    }
}
