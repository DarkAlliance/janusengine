﻿using SlimDX;
using System;
using System.Collections.Generic;
using System.IO;

namespace JanusEngine
{
    class Program
    {
        [STAThread]
        static void Main()
        {

            // Initialize the scene's Graphics engine & Physics engine 
            Renderer.Instance.Initialize();
            Physics.PhysicsWorld.Instance.Initalize();

            //// Create's the scene's objects
            //CreateSpheres(20);
            //CreatePlatforms();
            //CreatePlayer();
            //CreateWindowBounds();
            SceneLoader.Instance.LoadScene("scene.json");
           
            // Run the simulation
            Renderer.Instance.Run();
            Renderer.Instance.Shutdown();
        }
        /// <summary>
        /// Generates spheres with random sizes and colours
        /// </summary>
        /// <param name="count"></param>
        public static void CreateSpheres(int count)
        {
            // New Random var
            Random rand = new Random();

            // For each number of spheres required
            for (int i = 0; i < count; i++)
            {
                // Get a random number
                float radius = (float)rand.NextDouble();
                // Ensure the circle will be visible i.e the radius is not too small!
                while (radius < 0.5f)
                {
                    // Generate a new Random Float - Not the most elegant solution!
                    radius = (float)rand.NextDouble();
                }

                // Create a new Sphere Entity
                Entity Sphere = new Entity("Sphere" + i);
                
                // Add a transform component to the newly created sphere and populate it's position
                Transform2D transform = Sphere.AddComponent<Transform2D>();
                transform.Position = new Vector2((float)rand.NextDouble() * 10.0f, 9.0f);
                transform.Rotation = 0.0f;
                transform.Scale = new Vector2(1.0f, 1.0f);
                
                // Assign a render mesh to the Sphere with colour and shape
                MeshRenderer sphereMesh = Sphere.AddComponent<MeshRenderer>();
                sphereMesh.Colour = new Color4((float)(rand.NextDouble()), (float)(rand.NextDouble()), (float)(rand.NextDouble()));
                sphereMesh.Mesh = Mesh.CreateCircle(radius, 25).ToRenderMesh();

                // Add Physics component to the sphere 
                Physics.RigidBody spherePhysics = Sphere.AddComponent<Physics.RigidBody>();
                spherePhysics.Shape = new Physics.Circle(radius, 1.0f);
                spherePhysics.GravityInfluence = true;
                spherePhysics.Restitution = 0.5f;
                spherePhysics.Friction = 0.2f;
                //spherePhysics.Initalise();
                spherePhysics.ApplyImpulse(new Vector2((float)rand.NextDouble() * 10.0f, (float)rand.NextDouble()));
                Console.WriteLine("Sphere Generated :" + "\nName :" + Sphere.Name + "\nInitial Position :" + transform.Position.ToString());
                Sphere.Finish();
            }
        }
        /// <summary>
        /// Creates a Controllable Player object
        /// </summary>
        public static void CreatePlayer()
        {
            // Set Player's Entity name 
            Entity Player = new Entity("Player");

            // Set Player's Initial Transform
            Transform2D transform = Player.AddComponent<Transform2D>();
            transform.Position = new Vector2(0.0f, 9.0f);
            transform.Scale = new Vector2(1.0f, 1.0f);
            transform.Rotation = 0.0f;

            // Set Players Render Mesh
            MeshRenderer playerMesh = Player.AddComponent<MeshRenderer>();
            playerMesh.Colour = new Color4(0.1f, 1.0f, 1.0f);
            playerMesh.Mesh = Mesh.CreateRectangle(2.0f, 2.0f).ToRenderMesh();

            // Set Player's Physical Properties
            Physics.RigidBody playerPhysics = Player.AddComponent<Physics.RigidBody>();
            playerPhysics.Shape = new Physics.Rectangle(2.0f, 2.0f);
            //playerPhysics.Static = true;
            playerPhysics.GravityInfluence = true;
            playerPhysics.Restitution = 0.5f;
            playerPhysics.Friction = 0.2f;
         //   playerPhysics.Initalise();
            playerPhysics.ApplyImpulse(new Vector2(0.0f, 0.0f));

            // Set Player Controller - This means that this shape can be moved with the keys!
            Physics.PlayerController playerController = Player.AddComponent<Physics.PlayerController>();
            playerController.Speed = 10.0f;

            Console.WriteLine("Created Player - OK");
        }
        /// <summary>
        /// Generates Window Bounds of the Form / Window
        /// </summary>#
        ///<remarks>Uses four rectangles to generate the screen bounds, one box per side i.e. | _ </remarks>
        public static void CreateWindowBounds()
        {
            // Get aspect ratio & setup bounding volume properties
            float width = Renderer.Instance.aspectRatio * 20.0f;
            float height = 20.0f;
            const float half = 0.5f;
            const float extrude = 0.3f;
            Color4 colour = new Color4(0.2f, 0.0f, 1.0f);

            // Create four sides of the window
            CreateBox(new Vector2(-width * half, 0.0f), new Vector2(extrude, height), colour);
            CreateBox(new Vector2(width * half, 0.0f), new Vector2(extrude, height), colour);
            CreateBox(new Vector2(0.0f, height * half), new Vector2(width, extrude), colour);
            CreateBox(new Vector2(0.0f, -height * half), new Vector2(width, extrude), colour);
            Console.WriteLine("Created Window bounds - OK");
        }
        /// <summary>
        /// Generates the scene's platforms / Boxes 
        /// </summary>
        public static void CreatePlatforms()
        {
            // Create platform 1 -  Lowest platform
            CreateBox(new Vector2(0.0f, -5.0f), new Vector2(10.0f, 1.0f), new Color4(0.0f, 0.0f, 0.8f));
            // Create platform 2 - Middle platform
            CreateBox(new Vector2(5.0f, -3.0f), new Vector2(5.0f, 1.0f), new Color4(0.0f, 0.0f, 0.8f));
            // Create platform 3 - Highest platform
            CreateBox(new Vector2(1.0f, 1.0f), new Vector2(4.0f, 1.0f), new Color4(0.0f, 0.0f, 0.8f));
            Console.WriteLine("Created Platforms - OK");
        }
        /// <summary>
        /// Creates a general purpose Box
        /// </summary>
        /// <param name="position"></param>
        /// <param name="scale"></param>
        /// <param name="colour"></param>
        public static void CreateBox(Vector2 position, Vector2 scale, Color4 colour)
        {
            // Declare a new Box
            Entity Box = new Entity("Box");

            // Set Initial Transform
            Transform2D transform = Box.AddComponent<Transform2D>();
            transform.Position = position;
            transform.Scale = scale;
            transform.Rotation = 0.0f;

            // Set the render mesh for the box
            MeshRenderer boxMesh = Box.AddComponent<MeshRenderer>();
            boxMesh.Colour = colour;
            boxMesh.Mesh = Mesh.CreateRectangle(1.0f, 1.0f).ToRenderMesh();

            // Set the physics of the box
            Physics.RigidBody boxPhysics = Box.AddComponent<Physics.RigidBody>();
            boxPhysics.Shape = new Physics.Rectangle(scale.X, scale.Y);
            boxPhysics.Static = true;
            boxPhysics.Restitution = 1.0f;
            boxPhysics.Friction = 0.5f;
          //  boxPhysics.Initalise();
        }
    }
}
