﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JanusEngine
{
    /// <summary>
    /// Cant inherit
    /// </summary>
    public sealed class Entity
    {
        /// <summary>
        /// Name of the entity
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Parent of the entity
        /// </summary>
        public Entity Parent { get; set; }
        /// <summary>
        /// Every type has a type of class / map
        /// </summary>
        private IDictionary<Type, Component> components;
        /// <summary>
        ///  Creates an Entity
        /// </summary>
        /// <param name="name"></param>
        public Entity(string name = "Untitled")
        {
            Name = name;
            components = new Dictionary<Type, Component>();
        }
        /// <summary>
        /// Don't want people to add int, so it can only be of type component
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T AddComponent<T>() where T : Component
        {
            return AddComponent(typeof(T)) as T;
        }
        /// <summary>
        /// Don't want people to add int, so it can only be of type component
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public Component AddComponent(Type type) 
        {
            Component _component = Activator.CreateInstance(type, this) as Component;
            components.Add(type, _component);
           // _component.OnAttach();
            return _component;
        }
        /// <summary>
        /// Gets a component
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetComponent<T>() where T : Component
        {
            Type type = typeof(T);
            Component _component;

         if(components.TryGetValue(type,out _component))
         {
             return _component as T;
         }
         else
         {
             return null;
         }
        }
        /// <summary>
        /// Removes Component and calls Detach on said component
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public void RemoveComponent<T>() where T : Component
        {
            Type type = typeof(T);
            Component _component;

            if (components.TryGetValue(type, out _component))
            {
                _component.OnDetach();
                components.Remove(type);
            }
            // Log removed componnet
        }
        public void Finish()
        {
            foreach(var pair in components)
            {
                pair.Value.OnAttach();
            }
        }
    }
}
