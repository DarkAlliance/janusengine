cbuffer cPerFrame : register(b0)
{
	row_major float4x4 WVP;
	float4 Colour;
}

float4 main() : SV_TARGET
{
	return Colour;
}